# The PenlessRPG GM's Guide To Running A Game

Hello, and welcome to being a Game Master! While not more important than being a player,
being the GM means that you have a special role, a responsibility to your story and your players.
This responsibility is necessary to keep everything moving along smoothly, and is something
totally independant of the player's responsibility to their character and their fellow players.
Because while it is the player's job to be true to their character, while also being considerate
of the other player's fun, it is your job to give those characters context, and to give your players
oppurtunities to have fun at your game.

The tasks of a GM can be summarised roughly as follows:
1. To describe the world around the characters
2. To listen to your players' requests and determine whether, and how they can be fulfilled

These two tasks are what you'll be focussing on in every game, usually in that order.
I'll go into more detail about how, and why, for both of these individually.

## The Voice Of The World

As the GM, your main task is to give context to the characters that your players play. You are the
sole voice of the world they live in, describing the goings-on that they see, smell, hear, and touch.
That world can be one of your own creation, a setting from another story, or something made up on the fly.

Now, you obviously can't give players all of the information about everything going on at once,
nor can you feasibly write down every detail about every inch of an entire civilization.
So focus on the most important details first, most notably what's immidately around the characters.

Give the details they'd notice first, and focus on the important aspects of what's around them.
The fact that the shopkeep has a purple hat probably isn't as important as the fact that he's eyeing
the large jewel that the party just collected and looks like he's about to make an offer. That being said,
the purple hat might be important if this shopkeep is particularly eccentric, and more specifically
a fassion disaster who probably just wants to wear the diamond, thus having seemingly transparent motives.

The second thing to help with this is improvisation. Improv is the most important part of collaberative
storytelling, and often the backbone of the most memorable moments in a story. If you know roughly
what a location looks like, you can probably make it up as you go along from there (though writing it down
afterwards is good practice).

As for the specifics of your world, that's entirely up to you. You should give your players a world they'll
be interested in, but beyond that, just be creative. You can choose to involve them in the worldbuilding or not.
You can make a world bustling with commerse and people, or nearly dead after a series of disasters wrecked
the countryside. Fantasy, cyberpunk, modern, postmodern, it's up to you. If your players want a huge epic,
with major plot points and one final bad guy to fight, then go for it. If your players want to be dropped into
a world and carve out their own path, that's fine too.

## Herder Of Cats

Players like to do a lot of things, many of which require your intervention. Some of that intervention is easy,
like voicing an NPC's reaction, describing the next room, or simply saying that a given action is impossible.
Other times, it requires you to think about the logistics of a given action or ask your player for more details.
Sometimes you might even have to figure the perceptiveness of NPCs, the obsticles in the room, the character's
own adeptness, and sheer luck to be able to complete a given task.

But fret not, because there's a few (not so) secret tools at your disgression to make this job both possible and easy,
and we're going to take a look at them here.

### Randomness

The first tool in your toolbox is good old RNG. In PenlessRPG, we use coins to add a sense of risk to certain actions.
These can be anything from single coin flips for difficult actions, to entire minigames using multiple coin flips
in succesion. Some will lend themselves to needing multiple coin flips, such as for both an action's subtlety and
whether the chracter succeeds.

To give an example, say one of your players wants their character to pick a lock. First, you'll probably want
to ask them for a coin flip to see whether they can be quiet about it. Assuming they succeed that, they'll need
to actually pick the lock. You can just ask for a single coin flip, potentially giving them advantage or
disadvantage depending on their profficiencies and circumstance. Alternatively, you can use multiple coin flips
in succesion representing each of the pins. Which one, or whether you make one of your own, entirely depends
on the game and how much of a focus you want this to be.
