# License
[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

**PenlessRPG** and all content in this repository is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

# About
**PenlessRPG** is a minimalistic table-top roleplaying system based around coin flips and storytelling.
The base rules fit on a single sheet of paper and (with a little improvisation) are enough to tell stories in a fun and interactive manner.

## Special Thanks

* Thanks to Steven Zavala of flyovergames for suggesting coin flips instead of dice
* Thanks to my RPG group for putting up with my homebrew experiments like this

# Goals
1. Keep it simple to learn
2. Stay out of the way
3. Be easy to expand upon
4. Don't be redundant

# Getting Started
To get started, you'll need two things.

1. A Coin
2. [The Base Rules Sheet](base.md)
