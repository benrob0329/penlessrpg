# Optional Rules For PenlessRPG
These are optional rules that build atop the base rules sheet.
They aren't needed, but can add some extra fun depending on the game.
You can use all of these, or pick-and-choose which ones you like.

## Storytelling

**Story Points** let you make up your backstory on the fly to help resolve a situation.
You get 2 Story Points at the start of the game, you may spend 1 at any time to
add on to your character's history in order to move the story forward (such as
knowing a contact, having visited a location in the past, or having knowledge of
a rare kind). (Taken from BindRPG)

## Coin Mechanics

**Momentum** represents your party's collective energy when overcoming problems. Your party gains Momentum whenever you succeed on a Coin Flip. If you fail a Coin Flip, you may spend your Momentum to give yourself Advantage on that Coin Flip, keeping it on a success.

**Risk** is the equal reaction to Momentum. The GM gains a point of Risk whenever someone spends Momentum. The GM may use this for a variety of things, such as NPCs or environmental hazards.

## Extra Abilities

**Intuition** is your gut feeling, your innate notion of what to do. You can use
your sense of Intuition to help guide your actions, although you won't get any
specifics from it.

## Magic and Powers (These need some work)

**Thaumaturgy** is the art of taking energy from one source (such as a source of
combustion, a plant, or an actively moving object) and transfering it into
something else such as a fire, a block of ice, or a sword (likely in the form of heat).

Succeeding on a Thaumaturgy check means that you more-or-less complete the transfer,
most likely in the way you intended. Failing could result in anything from
a minor setback to a huge explotion. This transference of energy is easier
if the two sources are compatible (such as wood to a fire), already wanting
(such as heat to ice), or if it's very raw (such as heat to metal). Equalizing
is always easier than forcing more energy into something.

**Telekenisis** allows you to move and manipulate objects with your mind.

**Air Bending** is the art of manipulating the currents and force of the air
around you.

**Water Bending** is the art of manipulating the state, speed, and viscosity
of water, ice, and steam.

**Fire Bending** is the art of manipulating the size, shape, direction, and
intensity of fire and cumbustion.

**Light Bending** is the art of manipulating the brightness, area, and frequency
of light and other electromagnetic radiation.

**Amp Bending** is the art of manipulating electricity and it's flow through conductors.
