Hello, and welcome to PenlessRPG! This is a game about stories, specifically about telling them with friends.
To begin, you must first decide who will be The Game Master.

**The Game Master** (or GM) is the person who runs the game. Their job is to set up and manage the story,
while keeping all the Characters grounded within that. A game usually starts with the GM and Players deciding
where this story takes place, followed by each of the Players making a Character.

**Characters** are who the Players play throughout the story. Everyone is unique, so try to think about
where you came from, along with what your interests and morals are. Characters often face challenges, things which
they don't know for sure whether they'll succeed at or not. To represent this risk, we use Coin Flips.

**Coins Flips** are used when you want to do something risky or otherwise difficult. The GM may ask you to make
a Coin Flip at any time throughout the game. Now, it'd all be a bit boring if everyone's experience wasn't reflected
mechanically, so your Character's knowledge and experience is expressed through their Proficiencies.

**Proficiencies** are subjects your character knows a lot about. This can be anything from Bass Playing to Zoology.
Pick two Proficiencies for your Character at creation. If a Coin Flip is related to one of your Proficiencies, you gain Advantage.

**Advantage** is for when something is easier. It means that if you fail your Coin Flip the first time, you can try again.
Your GM may give any Coin Flip Advantage if they deem it fit, however they may also deem it fit to give it Disadvantage.

**Disadvantage** is for when something is extra difficult. It means you must make land two Heads in a row to succeed.
This is handy for if something (or someone) is making a task harder than it would normally be. One way to get around
Disadvantage is to do something with gives you Advantage, such as having someone Assist you.

**Assisting** grants someone Advantage on whatever they’re doing. Like double-teaming someone in a Fight.

**Fights** are usually between two parties. Flip a coin to see who goes first. Everyone can do one thing on their turn, but it must be quick. Attacking can be done through an Offensive Check.

**Offense Checks** are useful for when someone or something takes an action against a target. Make 3 coin flips, and your target must make a Defense Check, foiling your attempt on a success or succumbing to it on a fail.

**Defense Checks** are useful for when someone or something must defend against an action taken against them. Make 3 coin flips, you must meet or exceed the number of heads your offender landed in order to succeed.
