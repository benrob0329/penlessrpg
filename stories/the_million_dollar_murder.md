# Premise
The characters have been invited by a well-known multi-millionaire to a dinner
party to discuss matters of investment. The players may give their characters
any profession they want, within reason of course.

This story takes place in the mid-20th century.

# Characters
## John Banton
Well renowned multi-millionaire and business man. He is the one who has invited
the characters here in the first place. He takes interest in up-and-coming
industries and invests heavily into them. He has never been married, but has a
brother whom he has lost most contact with. Travels around a lot, has been
almost everywhere at some point in his life.

Proficiencies: Intuition, Business

Morals: Nonpracticing Catholic, he prefers to run an honest business but
isn't afraid to bend the rules a little if it means making a deal.

## Brett Bartholomew
Resident housekeeper. Originally from the UK, John offered him long-term
employment on his estate 7 years ago. He makes a full sweep of the house
over the course of a week. He thinks of himself rather highly, and believes
he is the smartest person in the room.

Proficiencies: Housekeeping, Logistics

Morals: Agnostic, meticulous rule-keeping and completion of tasks seems to keep
him sane.

## Butch Krandeler
Chef of the premises. Originally from Australia. Loves a hearty meal, almost as
much as his pet tarantula. John offered him the position after his restaurant
closed down due to competition from a larger establishment.

Proficiencies: Cooking, Heartiness

Morals: Baptist, won't work Sundays.

## Gregory Forn
Groundskeeper. Originally from Southern USA. Currently working in his retirement
on John's estate, which was a deal offered to him after he won a gardening
tournament with the Best In Show Flowers Arrangement.

Proficiencies: Gardening, Insightfullness

Morals: Practicing Catholic, likes being down-to-earth.

## George Banton
Brother of John Banton, hasn't had regular contact with him in years. He's
jealous of John's enterprise, but refuses to admit it. He's slippery, and
good at misdirection. He carries a gun on him at all times, but does not draw
attention to it.

Proficiencies: Misdirection, Firearms

Morals: Success is a dish best served cold, and behind someone else's back

# Outline
1. Arrival
	* Players arrive and John introduces the staff
	* Players are shown to their rooms
2. Dinner
	* Dinner is served by Butch, who then sits down and eats for himself
	* John talks to all the guests in a cheery manner, bringing up some things he found interesting about each one of their companies
	* Everyone finishes dinner feeling exhausted
3. Oversleep
	* Players find themselves awaking late into the afternoon, also finding that all of the staff have awoken late as well
	* Staff hastily prepare coffee and tea, along with a basic brunch while waiting for John to arrive, but he never does
4. Death Of John Banton
	* One of the staff members (probably Brett) will go to check up on John, finding him lying in bed, shot dead
	* An attempt may be made at this point to phone the police, however the lines have been cut
	* If the players came in vehicles, those are probably disabled somehow
5. Investigation
	* An investigation will be started, if not by one of the players then one of the staff will push the idea
6. Arrival Of George Banton
	* At some point that evening, George will arrive at the mansion. None of the staff will have been aware of his travel however a letter can be found in John's desk stating that he would be coming to visit.
	* George will act astounded that anyone would kill his brother, and will claim to assist the players in investigation, while actually trying to throw them off
7. George Snaps
	* During the investigation, George will probably be found out. If he is discovered, he'll snap in a last-ditch effort to salvage his plan. He make pull a gun, he may try to run, he might even kidnap someone. It should be obvious that he is desperate, but also ready to do anything to get what he wants (which may be either freedom or the enterprise, depending on how much is found out).
8. Catching The Killer
	* The players will need to either catch/kill George and stop him escaping, or contact the police so that they arrive and manage to aid them in arresting him.

# The Mansion
## Rooms
* Ground Floor
	* Foyer
	* Dining Room
		* Secret Passageway B
	* Kitchen
	* Butch's Room
	* Study/Library
	* Lounge
		* Secret Passageway C
* Upper Floor
	* Master Bedroom
		* Secret Passageway B
	* Brett's Room
	* North Guest Room
	* South Guest Room
		* Secret Passageway C
	* East Guest Room
	* West Guest Room
* Basement
	* Cold Storage
	* Secret Passageway A
* Outer Grounds
	* Courtyard
	* Garden
	* Gregory's Shed
	* Backwoods
		* Secret Passageway A

## Clues
* There is a painting in the Study which seems to be rather tilted, on a closer look it is revealed to be hiding an in-wall safe.

* Inside the safe there is a will signed "John Banton" but on close inspection the writing is not quite the same as John's other letters. The will grants John's brother, George everything in his estate, including his business and investments.

* There is a letter inside the desk in the study that is from John's brother, stating that he will be arriving that evening

* There is blood on the wall of the Master Bedroom, a closer look reveals Secret Passageway B

* The passageways have been recently disturbed, with footprints going in and out through the dust.

* There is a bottle of sleeping medication inside Secret Passageway B

## George's Steps
1. Sneak in through Secret Passageway A
2. Plant Sleeping Medication in the dinner
3. While everyone is sleeping, shoot John with a pistol under the pillow
4. Plant the fake will inside John's safe
5. On his way through Secret Passageway B, he accidentally cuts himself on the panel and drops the sleeping medication
6. Cut phone lines and disable vehicles
7. Camp in the woods until the following evening
8. Cover his tracks with a plausible story and well-placed misdirection
